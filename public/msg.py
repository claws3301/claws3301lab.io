from flask import Flask, request, render_template, redirect, url_for

app = Flask(__name__)

msgs = []        #留言采用字典

@app.route('/' , methods = ["GET", "POST"])
def index():
    if request.method == 'GET':
        return render_template('index.html', msgs=msgs)

    else:
        msg = request.form.get('msg')
        uname = request.form.get('uname')

        msgs.append({"msg": msg,
                    "uname": uname})
        return redirect(url_for('index'))



if __name__ == '__main__':
    app.run()